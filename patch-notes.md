# Patch notes

## Version 1.1.1

*Released 2021 March 16.*

- Increased maximum players from 8 to 12.
- Fixed certain heroes losing their scoped or special primary fire during the
  Technical and Showstopper Challenges.
- Fixed players getting stuck as the Technical Challenge hero in the Showstopper
  Challenge when they took a longer time in hero selection.

## Version 1.1.0

*Released 2021 March 5.*

### General

- Faster ultimate charge wasn't actually happening in the Showstopper Challenge,
  but now it is.
- The duration of each challenge in minutes can now be configured.
- Recognition of Soggy Bottoms, and the duration of their immortality, can now
  be configured.
- Recognition of Bingates, and the bonus points rewarded, can now be configured.

### Heroes

- D.Va wasn't working too well in the Technical Challenge, so she will no longer
  be chosen for it.
- Baby D.Va was stuck out of mech with no offense. Now her Light Gun works and
  she can Call Mech once it charges.
- The cooldown for Doomfist's Rocket Punch was too long and has been dialed back.
- McCree's Deadeye was not dealing its full damage, but is now back to 100%.

### Testing

- Testing features are now controlled by workshop settings.
- A new Interact+Crouch command, for any player, deals 1000 self damage.
- Self-damage deaths can now be optionally recognized for testing.

## Version 1.0.0

*Released 2021 March 1.*

- Initial release
