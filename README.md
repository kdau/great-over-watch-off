# ![[icon]](promo/icon.png) Great Over Watch Off

**Creator:** kdau#1430
**Workshop code:** `2177F`

A three-round Overwatch deathmatch mode inspired by 🇬🇧🍞😉 [a certain UK baking program](https://en.wikipedia.org/wiki/The_Great_British_Bake_Off). Compete in signature, technical and showstopper challenges. Don't be the soggy bottom or cause a Bingate...or should you?

You'll choose one hero for the match. Some heroes are unavailable because their kits don't fit with this mode's restrictions. Many of the rest have been rebalanced for fairer combat under those restrictions.

## Challenges

Each of the three challenge rounds lasts for three minutes:

1. **Signature Challenge**: You won't have your hero's primary weapon fire, but you'll have any secondary or scoped fire. Your ability cooldowns will be much shorter, but your ultimate ability won't be available. No melee punches except for Tracer and Winston.

2. **Technical Challenge**: All players will switch to the same random hero - one that none of them were already playing. Otherwise, the rules are the same as the Signature Challenge.

3. **Showstopper Challenge**: You will switch back to your original hero. Your ultimate ability will now be available, starting fully charged and recharging much faster.

## Distinctions

- **Soggy Bottom**: In the Signature and Technical Challenges, the player with the most deaths at any time will be the Soggy Bottom. This player gets wallhacks (like Widowmaker's ultimate) for the duration of this dubious honor. They will also be immortal for the first 15 seconds, as shown by a blue aura around them.

- **Bingate**: In the Showstopper Challenge, you can score a bonus point for each Bingate you perform: an ultimate shutdown caused by dealing a final blow to a player who is using their (interruptable) ultimate.

- **Star Baker**: After all three challenges, the player with the highest cumulative score is the Star Baker of the match. Just like on the show, the only prize is the admiration of your peers.

## Settings

The host player can change the duration of the challenges. They can also adjust whether Soggy Bottoms and Bingates are recognized and the bonuses that come with them.

## Source

This mode's source code is available in its [GitLab repository](https://gitlab.com/kdau/great-over-watch-off). It is developed using the [OSTW](https://github.com/ItsDeltin/Overwatch-Script-To-Workshop/) high-level scripting language.
